#ifndef _DETECT_FACE_H_
#define _DETECT_FACE_H_

#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>

using namespace cv;
using namespace std;

class DetectFace
{
public:
	DetectFace(Rect maxRect, Rect initialPosition, CascadeClassifier* cascade);
	~DetectFace();

	Rect getFacePosition(Mat wholeImage);
	Rect doubleSize(Rect curr);
	Mat getFaceTemplate(const Mat &frame, Rect face);

private:
	Rect maxRect;
	Rect lastFound;
	Rect lastArea;
	bool foundFace = false;
	vector<Rect> faces;

	bool isTemplate = false;
	Mat faceTemplate;
	Mat templateResult;

	CascadeClassifier* cascade;

};


#endif