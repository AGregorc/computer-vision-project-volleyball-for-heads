#include "Wall.h"
#include <iostream>
#include "GameLogic.h"

Wall::Wall(GameLogic *game1, int type, float posX, float posY, float sizeX, float sizeY) : Entity(game1, type) {
	body = NULL;

	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(posX, posY);
	body = game->world->CreateBody(&groundBodyDef);
	b2PolygonShape groundBox;
	groundBox.SetAsBox(sizeX, sizeY);
	body->CreateFixture(&groundBox, 0.0f);

	body->SetUserData(this);

	std::cout << "Wall is created" << std::endl;
}

int Wall::getEntityType() {
	return type;
}

Wall::~Wall() {
}

Wall* Wall::create(GameLogic *game1, int type, float posX, float posY, float sizeX, float sizeY) {
	Wall *objWall = new Wall(game1, type, posX, posY, sizeX, sizeY);
	if (objWall)
		return objWall;
	std::cout << "Could not create a wall..." << std::endl;
	return NULL;
}