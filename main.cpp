#include "GameLogic.h"
#include "DetectFace.h"

using namespace std;
using namespace cv;

float sizeX;
float sizeY;


const int w = 500;
int levels = 3;

CascadeClassifier cascade;
vector<Point2f> points1_now, points1_previous;
vector<vector<Point>> contours;
vector<Vec4i> hierarchy;


int main(int argc, char** argv)
{
	B2_NOT_USED(argc);
	B2_NOT_USED(argv);

	TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	if (!cascade.load("lbpcascade_frontalface.xml")) {
		cerr << "Could not load classifier cascade" << endl;
		return -1;
	}

	VideoCapture camera(0); // ID of the camera

	if (!camera.isOpened()) {
		cerr << "Unable to access camera" << endl;
		return -1;
	}

	// Define Mat objects.
	Mat cap, frame, gray, gray1_now, gray1_previous, region1, region2, gray1, gray2, image1, image2, helmet1, helmet2, helmetMask;

	camera >> frame;

	if (frame.empty()) {
		cerr << "Frame is empty" << endl;
		return -1;
	}

	sizeX = frame.cols;
	sizeY = frame.rows;

	cout << "size: " << sizeX << " " << sizeY << endl;

	image1 = imread("helmet1.png", CV_LOAD_IMAGE_COLOR);
	image2 = imread("helmet2.png", CV_LOAD_IMAGE_COLOR);
	Size helmetSize(sizeX / 5, sizeX / 10);
	resize(image1, helmet1, helmetSize);
	resize(image2, helmet2, helmetSize);
	cout << "Helmet size: " << sizeX / 5 << " " << sizeX / 10 << endl;
	cout << "Helmet size: " << helmet1.cols << " " << helmet1.rows << endl;

	threshold(helmet1, helmetMask, 0, 50, THRESH_BINARY); //Threshold the helmet
														  //imshow("hemletMask", helmetMask);

	cvtColor(helmet1, helmetMask, CV_BGR2GRAY);

	vector<vector<Point> > contours0;
	findContours(helmetMask, contours0, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	contours.resize(contours0.size());
	for (size_t k = 0; k < contours0.size(); k++) {
		cout << "Contours " << k << endl;
		approxPolyDP(Mat(contours0[k]), contours[k], 1, true);
	}

	const int num = contours[0].size();
	b2Vec2 *p1Vertices = new b2Vec2[num];
	b2Vec2 *p2Vertices = new b2Vec2[num];
	int p1Count = num;
	int p2Count = num;

	for (int i = 0; i < num; i++) {
		Point p = contours[0][i];
		cout << "x: " << p.x << " y: " << p.y << endl;
		p1Vertices[i].Set(p.x/PPM, p.y/PPM);
		p2Vertices[i].Set(p.x / PPM, p.y / PPM);
	}

	GameLogic *gameLogic = GameLogic::create(sizeX, sizeY, p1Vertices, p2Vertices, p1Count, p2Count);

	namedWindow(WINDOW_NAME, WINDOW_AUTOSIZE | WINDOW_KEEPRATIO | WINDOW_GUI_EXPANDED);
	vector<Rect> faces;
	vector<Rect> faces1;
	vector<Rect> faces2;

	int rect_size = 70;
	float scale_factor = 1.05;
	int min_neighbours = 1;
	int offset = 1;
	int64 debug[] = { 0, 0, 0, 0, 0 };
	int64 delta = getTickCount();

	int p1X = sizeX / 4;
	int p1Y = sizeY * 3 / 4;
	Point2f p1median = Point2f(0, 0);
	DetectFace* p1Face = new DetectFace(Rect(0, 0, sizeX * 6 / 10, sizeY), Rect(0, p1Y - sizeY / 4, sizeX / 2, sizeY / 2), &cascade);

	int p2X = sizeX * 3 / 4;
	int p2Y = sizeY * 3 / 4;
	Point2f p2median = Point2f(0, 0);
	DetectFace* p2Face = new DetectFace(Rect(sizeX * 4 / 10, 0, sizeX * 6 / 10, sizeY), Rect(sizeX * 4 / 10, p2Y - sizeY / 4, sizeX / 2, sizeY / 2), &cascade);

	
	while (true)
	{
		int64 t = 0;

		t = getTickCount();
		camera >> cap;
		flip(cap, frame, 1);

		int64 t2 = getTickCount();
		gameLogic->update((double)(t2 - delta) / 2 / getTickFrequency());
		cout << (double)1/((t2 - delta) / (double)getTickFrequency()) << endl;
		delta = getTickCount();

		// Get position and angle of the body.
		b2Vec2 ballPosition = gameLogic->ball->body->GetPosition();
		//float32 ballAngle = gameLogic->ball->body->GetAngle();
		//cout << "Ball position: " << ballPosition.x << " " << ballPosition.y << endl;

		// Get position of players
		b2Vec2 p1Position = gameLogic->player1->body->GetPosition();
		//float32 p1Angle = player1body->GetAngle();
		b2Vec2 p2Position = gameLogic->player2->body->GetPosition();
		t = getTickCount() - t;
		debug[0] += t;

		t = getTickCount();

		Rect p1 = p1Face->getFacePosition(frame);
		b2Vec2 vel1(((p1.x + (p1.width - helmet1.cols) / 2) / PPM - p1Position.x), ((p1.y - helmet1.rows) / PPM - p1Position.y));
		gameLogic->player1->body->SetLinearVelocity(vel1);

		p1Position.x = (p1.x + (p1.width - helmet1.cols) / 2) / PPM;
		p1Position.y = (p1.y - helmet1.rows) / PPM;
		gameLogic->player1->body->SetTransform(p1Position, 0);

		Rect p2 = p2Face->getFacePosition(frame);
		b2Vec2 vel2(((p2.x + (p2.width - helmet1.cols) / 2) / PPM - p2Position.x), ((p2.y - helmet1.rows) / PPM - p2Position.y));
		gameLogic->player2->body->SetLinearVelocity(vel2);

		p2Position.x = (p2.x + (p2.width - helmet2.cols) / 2) / PPM;
		p2Position.y = (p2.y - helmet2.rows) / PPM;
		gameLogic->player2->body->SetTransform(p2Position, 0);

		t = getTickCount() - t;
		debug[1] += t;
		
		t = getTickCount();

		// TODO: neki zih manjka :D
		
		t = getTickCount() - t;
		debug[2] += t;

		t = getTickCount();

		// checking if hats are out of bounds
		if (p1Position.x * PPM < 0) {
			//cout << "x: " << p1Position.x << endl;
			p1Position.x = (offset) / PPM;
			gameLogic->player1->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player1->body->SetTransform(p1Position, 0);
		}
		else if (p1Position.x * PPM > (sizeX - helmet1.cols) / 2) {
			//cout << "x: " << p1Position.x << endl;
			p1Position.x = ((sizeX - helmet1.cols) / 2 - offset) / PPM;
			gameLogic->player1->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player1->body->SetTransform(p1Position, 0);
		}
		if (p1Position.y * PPM < 0) {
			//cout << "y: " << p1Position.y << endl;
			p1Position.y = (offset) / PPM;
			gameLogic->player1->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player1->body->SetTransform(p1Position, 0);
		}
		else if (p1Position.y * PPM > sizeY - helmet1.rows) {
			//cout << "y: " << p1Position.y << endl;
			p1Position.y = (sizeY - helmet1.rows - offset) / PPM;
			gameLogic->player1->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player1->body->SetTransform(p1Position, 0);
		}

		if (p2Position.x * PPM > sizeX - helmet2.cols) {
			//cout << "x: " << p2Position.x << endl;
			p2Position.x = (sizeX - helmet2.cols - offset) / PPM;
			gameLogic->player2->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player2->body->SetTransform(p2Position, 0);
		}
		else if (p2Position.x * PPM < (sizeX - helmet1.cols) / 2) {
			//cout << "x: " << p2Position.x << endl;
			p2Position.x = ((sizeX - helmet1.cols) / 2 + offset) / PPM;
			gameLogic->player2->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player2->body->SetTransform(p2Position, 0);
		}
		if (p2Position.y * PPM < 0) {
			//cout << "y: " << p2Position.y << endl;
			p2Position.y = ( offset) / PPM;
			gameLogic->player2->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player2->body->SetTransform(p2Position, 0);
		}
		else if (p2Position.y * PPM > sizeY - helmet1.rows) {
			//cout << "y: " << p2Position.y << endl;
			p2Position.y = (sizeY - helmet1.rows - offset) / PPM;
			gameLogic->player2->body->SetLinearVelocity(b2Vec2(0, 0));
			gameLogic->player2->body->SetTransform(p2Position, 0);
		}
		t = getTickCount() - t;
		debug[3] += t;

		t = getTickCount();

		rectangle(frame, Point((sizeX - NET_WIDTH) / 2, sizeY), Point((sizeX + NET_WIDTH) / 2, sizeY / 2), Scalar(255, 0, 100), -1);
		helmet1.copyTo(frame(Rect(Point(p1Position.x * PPM , p1Position.y * PPM ), helmetSize)), helmetMask);
		helmet2.copyTo(frame(Rect(Point(p2Position.x * PPM , p2Position.y * PPM ), helmetSize)), helmetMask);
		circle(frame, Point(ballPosition.x * PPM, ballPosition.y * PPM), RADIUS, Scalar(255, 0, 255), -1);
		putText(frame, to_string(gameLogic->player1Score) + ":" + to_string(gameLogic->player2Score), Point(sizeX / 2 - 50, 50), FONT_HERSHEY_SIMPLEX, 2, Scalar(255, 255, 255), 3);
		imshow(WINDOW_NAME, frame);

		t = getTickCount() - t;
		debug[4] += t;
		//imshow("Helmet", helmet);
		char c = waitKey(1);
		if (c == 'p') {
			for (int i = 0; i < 5; i++) {
				cout << "Debug part: " << i << " " << debug[i] << endl;
			}
		}
	}

	return 0;
}
