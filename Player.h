#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <Box2D\Box2D.h>
#include "Entity.h"

class Player : public Entity
{
public:
	b2Body* body;

	Player(GameLogic *game1, int type, float sizeX, float sizeY, float radius, b2Vec2 *vertices, int count);
	~Player();

	int getEntityType();

	static Player* create(GameLogic *game1, int type, float sizeX, float sizeY, float radius, b2Vec2 *vertices, int count);
};

#endif