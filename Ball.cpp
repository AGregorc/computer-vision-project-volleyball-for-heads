#include "Ball.h"
#include <iostream>

#include "GameLogic.h"


Ball::Ball(GameLogic *game1, int type, float sizeX, float sizeY, float radius):Entity(game1, type) {
	body = NULL;

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(sizeX, sizeY);
	body = game->world->CreateBody(&bodyDef);
	b2CircleShape dynamicBox;
	dynamicBox.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 1.f;
	body->CreateFixture(&fixtureDef);

	body->SetUserData(this);
	std::cout << "New ball is created" << std::endl;
}

Ball::~Ball(void) {

}

int Ball::getEntityType() {
	return type;
}

Ball* Ball::create(GameLogic *game1, int type, float sizeX, float sizeY, float radius) {
	Ball *objBall = new Ball(game1, type, sizeX, sizeY, radius);
	if (objBall)
		return objBall;
	std::cout << "Could not create ball..." << std::endl;
	return NULL;
}