#ifndef __ENTITY_GAME_W__
#define __ENTITY_GAME_W__

class GameLogic;

class Entity
{
public:
	GameLogic *game;
	int type;

	Entity(GameLogic *gl, int type);
	~Entity();
	virtual int getEntityType() = 0;
};

#endif
