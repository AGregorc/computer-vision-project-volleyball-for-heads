#include "GameLogic.h"


GameLogic::GameLogic(float sizeX, float sizeY, b2Vec2 *p1V, b2Vec2 *p2V, int p1C, int p2C) {
	b2Vec2 gravity(0.0f, 10.0f);
	world = new b2World(gravity);
	_sizeX = sizeX;
	_sizeY = sizeY;
	p1Vertices = p1V;
	p2Vertices = p2V;
	p1Count = p1C;
	p2Count = p2C;
	nextTouch = TIME_TO_NEXT_TOUCH;

	// Define the ground body.
	groundP1Body = Wall::create(this, eBottomP1, 0, sizeY / PPM, sizeX / 2 / PPM, 0);
	groundP2Body = Wall::create(this, eBottomP2, sizeX / PPM, sizeY / PPM, sizeX / 2 / PPM, 0);
	//groundBody = Wall::create(world, 0, sizeY / PPM, sizeX / PPM, 0);

	// Define the ceiling body.
	ceilingBody = Wall::create(this, eCeilingWall, 0, 0, sizeX / PPM, 0);

	// Define the left wall body.
	leftBody = Wall::create(this, eLeftWall, 0, 0, 0, sizeY / PPM);

	// Define the rigth wall body.
	rigthBody = Wall::create(this, eRightWall, sizeX / PPM, 0, 0, sizeY / PPM);

	// Define net body.
	netBody = Wall::create(this, eNet, (sizeX - NET_WIDTH) / 2 / PPM, sizeY / PPM, NET_WIDTH / PPM, sizeY / 2 / PPM);

	// Define the dynamic body (ball). We set its position and call the body factory.
	ball = Ball::create(this, eBall, sizeX / 2 / PPM, sizeY / 4 / PPM, RADIUS / PPM);

	// Define the player 1 body. We set its position and call the body factory.
	player1 = Player::create(this, ePlayer1, (sizeX / 4) / PPM, (3 * sizeY / 4) / PPM, sizeX / 20 / PPM, p1Vertices, p1Count);

	// Define the player 2 body. We set its position and call the body factory.
	player2 = Player::create(this, ePlayer2, (3 * sizeX / 4) / PPM, (3 * sizeY / 4) / PPM, sizeX / 20 / PPM, p2Vertices, p2Count);

	world->SetContactListener(&myContactListenerInstance);

	if (ball == NULL || player1 == NULL || player2 == NULL || rigthBody == NULL) {
		cout << "nekej je NULL !!!" << endl;
		while (true) {

		}
	}

	player1Score = 0;
	player2Score = 0;
	reset();

	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	timeStep = 1.0f / 60.0f;
	velocityIterations = 8;
	positionIterations = 3;
}

void GameLogic::update(double delta ) {
	if (countToStart > 0) {
		countToStart -= timeStep;
		if (setTransform) {
			ball->body->SetTransform(b2Vec2(_sizeX / 2 / PPM, _sizeY / 4 / PPM), 0);
			setTransform = false;
		}
		//cout << "Position: " << _sizeX / 2 / PPM << " " << _sizeY / 4 / PPM << endl;
		//b2Vec2 pos = ball->body->GetPosition();
		//cout << "Real position: " << pos.x << " " << pos.y << endl;

	}
	else {
		// Instruct the world to perform a single step of simulation.
		// It is generally best to keep the time step and iterations fixed.
		world->Step(delta, velocityIterations, positionIterations);
		nextTouch--;
		//world->DrawDebugData();
	}
	//cout << timeStep << " " << velocityIterations << " " << positionIterations << " "<< world << endl;
}

void GameLogic::reset() {
	countToStart = 1;
	setTransform = true;
	if (rand() % 2 == 0)
		ball->body->SetLinearVelocity(b2Vec2(2, 0));
	else
		ball->body->SetLinearVelocity(b2Vec2(-2, 0));
}

GameLogic* GameLogic::create(float sizeX, float sizeY, b2Vec2 *p1Vertices, b2Vec2 *p2Vertices, int p1Count, int p2Count) {
	GameLogic* gl = new GameLogic(sizeX, sizeY, p1Vertices, p2Vertices, p1Count, p2Count);
	if (gl)
		return gl;
	cout << "Could not create game logic" << endl;
	return NULL;
}

GameLogic::~GameLogic() {

}