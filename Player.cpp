#include "Player.h"
#include <iostream>
#include "GameLogic.h"

Player::Player(GameLogic *game1, int type, float sizeX, float sizeY, float radius, b2Vec2 *vertices, int count) : Entity(game1, type) {
	b2Vec2 pPosition(sizeX, sizeY);
	b2BodyDef helmetDef;
	helmetDef.type = b2_kinematicBody;
	helmetDef.position.Set(pPosition.x, pPosition.y);
	body = game->world->CreateBody(&helmetDef);
	b2PolygonShape polygonShape;
	b2FixtureDef pfixtureDef;
	pfixtureDef.shape = &polygonShape;
	pfixtureDef.density = 1.0f;
	pfixtureDef.friction = 0.3f;
	//polygonShape.Set(vertices, count);

	int numOfFixtures = ((count - 1)/ 8) + 1;
	int previus = 0;
	for (int i = 0; i < numOfFixtures; i++) {
		int numOfEdges = count - previus;
		if (numOfEdges < 14 && numOfEdges > 8) numOfEdges /= 2;
		if (numOfEdges > 8) numOfEdges = 8;
		if (numOfEdges < 3) {
			std::cout << "num of edges is: " << numOfEdges << std::endl;
			break;
		}

		b2Vec2 *points = new b2Vec2[numOfEdges];
		
		for (int j = 0; j < numOfEdges; j++) {
			points[j].Set(vertices[previus + j].x, vertices[previus + j].y);
			std::cout << "Point: " << vertices[previus + j].x << " : " << vertices[previus + j].y << std::endl;
		}
		polygonShape.Set(points, numOfEdges);
		body->CreateFixture(&pfixtureDef);

		previus += numOfEdges;
	}
	//p1fixtureDef.restitution = 0.8f;

	body->SetUserData(this);

	std::cout << "Player is created" << std::endl;
}


Player::~Player() {
}


int Player::getEntityType() {
	return type;
}

Player* Player::create(GameLogic *game1, int type, float sizeX, float sizeY, float radius, b2Vec2 *vertices, int count) {
	Player *objPlayer = new Player(game1, type, sizeX, sizeY, radius, vertices, count);
	if (objPlayer)
		return objPlayer;

	std::cerr << "Could not create player..." << std::endl;
	return NULL;
}