#ifndef __GAMELOGIC_H__
#define __GAMELOGIC_H__

#include <Box2D/Box2D.h>

#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <stdlib.h> 

#include <CollisionListener.h>
#include "Ball.h"
#include "Player.h"
#include "Wall.h"

#define WINDOW_NAME "Volleyball on hats"
#define RADIUS 30	// ball radius
#define NET_WIDTH 10.0f
#define PPM 100.0f //Pixels per meter
#define MAX_COUNT 100
#define TIME_TO_NEXT_TOUCH 5

using namespace std;

enum {
	ePlayer1 = 1,
	ePlayer2,
	eBall,
	eBottomP1,
	eBottomP2,
	eNet,
	eLeftWall,
	eRightWall,
	eCeilingWall
};

class GameLogic  
{
public:
	b2World* world;

	b2Vec2 *p1Vertices;
	b2Vec2 *p2Vertices;
	int p1Count;
	int p2Count;

	Player* player1;
	Player* player2;

	int nextTouch;

	Ball* ball;
	Wall* netBody;

	int player1Score;
	int player2Score;

	GameLogic(float sizeX, float sizeY, b2Vec2 *p1Vertices, b2Vec2 *p2Vertices, int p1Count, int p2Count);
	~GameLogic();

	void reset();
	void update(double delta);

	static GameLogic* create(float sizeX, float sizeY, b2Vec2 *p1Vertices, b2Vec2 *p2Vertices, int p1Count, int p2Count);

private:
	float _sizeX;
	float _sizeY;

	Wall* groundP2Body;
	Wall* groundP1Body;
	Wall* groundBody;
	Wall* ceilingBody;
	Wall* leftBody;
	Wall* rigthBody;

	CollisionListener myContactListenerInstance;

	float32 timeStep;
	int32 velocityIterations;
	int32 positionIterations;

	
	float countToStart;
	bool setTransform;
};


#endif 
