#include "DetectFace.h"
#include <iostream>

DetectFace::DetectFace(Rect mR, Rect initialPosition, CascadeClassifier* casc)
{
	maxRect = mR;
	lastArea = initialPosition;
	lastFound = initialPosition;
	lastFound.width /= 2;
	lastFound.height /= 2;
	cascade = casc;
}

DetectFace::~DetectFace()
{
}

Rect DetectFace::doubleSize(Rect curr) {
	Rect result;

	// Double rect size
	result.width = curr.width * 2;
	result.height = curr.height * 2;

	// Center rect around original center
	result.x = max(curr.x - curr.width / 2, maxRect.x);
	result.y = max(curr.y - curr.height / 2, maxRect.y);

	if (result.x + result.width > maxRect.x + maxRect.width) {
		result.width = maxRect.x + maxRect.width - result.x;
	}
	if (result.y + result.height > maxRect.y + maxRect.height) {
		result.height = maxRect.y + maxRect.height - result.y;
	}
	return result;
}

Mat DetectFace::getFaceTemplate(const Mat &frame, Rect face)
{
	face.x += face.width / 4;
	face.y += face.height / 4;
	face.width /= 2;
	face.height /= 2;

	cv::Mat faceTemplate = frame(face).clone();
	return faceTemplate;
}

Rect DetectFace::getFacePosition(Mat frame) {
	Mat region = frame(lastArea);
	Mat gray;
	cvtColor(region, gray, COLOR_BGR2GRAY);

	cascade->detectMultiScale(gray, faces, 1.1, 3, 0,
		cv::Size(lastFound.width * 8 / 10, lastFound.height * 8 / 10),
		cv::Size(lastFound.width * 12 / 10, lastFound.width * 12 / 10));

	rectangle(frame, lastArea, Scalar(140, 120, 222));

	int closestIndex1 = -1;
	int distanceTo1 = INT_MAX;
	for (unsigned int i = 0; i < faces.size(); i++) {
		Rect r = faces[i];
		r.x += lastArea.x;
		r.y += lastArea.y;

		int dx1 = r.x - lastFound.x;
		int dy1 = r.y - lastFound.y;
		double dist1 = sqrt(dx1*dx1 + dy1*dy1);
		//cout << dist1 << " " << dist2 << endl;

		if (dist1 < distanceTo1) {
			closestIndex1 = i;
			distanceTo1 = dist1;
		}
	}
	if (closestIndex1 > -1) {
		Rect r = faces[closestIndex1];
		r.x += lastArea.x;
		r.y += lastArea.y;

		lastFound = r;
		lastArea = doubleSize(lastFound);
		faceTemplate = getFaceTemplate(frame, lastFound);
		isTemplate = true;

		rectangle(frame, Point(round(r.x), round(r.y)),
			Point(round(r.x + r.width - 1), round(r.y + r.height - 1)),
			Scalar(0, 255, 255), 3, 8, 0);

		return lastFound;
	}
	else if (isTemplate) {
		matchTemplate(frame(lastArea), faceTemplate, templateResult, CV_TM_SQDIFF_NORMED);
		normalize(templateResult, templateResult, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

		double min, max;
		cv::Point minLoc, maxLoc;
		cv::minMaxLoc(templateResult, &min, &max, &minLoc, &maxLoc);

		// Add roi offset to face position
		minLoc.x += lastArea.x;
		minLoc.y += lastArea.y;

		// Get detected face
		lastFound = Rect(minLoc.x, minLoc.y, faceTemplate.cols, faceTemplate.rows);
		lastFound = doubleSize(lastFound);

		// Get new face template
		faceTemplate = getFaceTemplate(frame, lastFound);

		lastArea = doubleSize(lastFound);


		// draw rectangle
		Rect r = lastFound;
		rectangle(frame, Point(round(r.x), round(r.y)),
			Point(round(r.x + r.width - 1), round(r.y + r.height - 1)),
			Scalar(0, 255, 0), 3, 8, 0);

	}

	return lastFound;
}
