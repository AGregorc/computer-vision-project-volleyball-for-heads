#ifndef _WALL_H_
#define _WALL_H_

#include <Box2D\Box2D.h>
#include "Entity.h"

class Wall : public Entity {

public:
	b2Body* body;

	Wall(GameLogic *game1, int type, float posX, float posY, float sizeX, float sizeY);
	~Wall();

	int getEntityType();

	static Wall* create(GameLogic *game1, int type, float posX, float posY, float sizeX, float sizeY);

};

#endif
