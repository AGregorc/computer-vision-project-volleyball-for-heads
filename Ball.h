#ifndef _BALL_H_
#define _BALL_H_

#include <Box2D\Box2D.h>
#include "Entity.h"

class Ball : public Entity
{
public:
	b2Body* body;

	Ball(GameLogic *game1, int type, float sizeX, float sizeY, float radius);
	~Ball();

	int getEntityType();

	static Ball* create(GameLogic *game1, int type, float sizeX, float sizeY, float radius);

};

#endif
