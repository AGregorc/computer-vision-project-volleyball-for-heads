#include "CollisionListener.h"
#include <iostream>
#include "GameLogic.h"

using namespace std;


void CollisionListener::BeginContact(b2Contact* contact) {
	//cout << "A misls, da je ratal? Bomo vidl" << endl;
	//check if fixture A was a ball
	Entity * bodyUserData1 = (Entity *) contact->GetFixtureA()->GetBody()->GetUserData();
	Entity * bodyUserData2 = (Entity *) contact->GetFixtureB()->GetBody()->GetUserData();

	if (bodyUserData1 && bodyUserData2) {
		if (bodyUserData2->getEntityType() == eBall) {
			Entity * tmp = bodyUserData1;
			bodyUserData1 = bodyUserData2;
			bodyUserData2 = tmp;
		}

		if (bodyUserData1->getEntityType() == eBall) {
			int type2 = bodyUserData2->getEntityType();

			if (type2 == eBottomP1) {
				bodyUserData1->game->player2Score++;
				bodyUserData1->game->reset();
				p1touches = 0;
				p2touches = 0;
				cout << "Player 1 lost" << endl;
			}
			else if (type2 == eBottomP2) {
				bodyUserData1->game->player1Score++;
				bodyUserData1->game->reset();
				p1touches = 0;
				p2touches = 0;
				cout << "Player 2 lost" << endl;
			}
			else if (type2 == ePlayer1 && bodyUserData1->game->nextTouch < 0) {
				bodyUserData1->game->nextTouch = TIME_TO_NEXT_TOUCH;
				p1touches++;
				p2touches = 0;
				cout << "P1 Touches the ball " << p1touches << endl;
				if (p1touches >= 4) {
					bodyUserData1->game->player2Score++;
					bodyUserData1->game->reset();
					p1touches = 0;
				}
			}
			else if (type2 == ePlayer2 && bodyUserData1->game->nextTouch < 0) {
				bodyUserData1->game->nextTouch = TIME_TO_NEXT_TOUCH;
				p2touches++;
				p1touches = 0;
				cout << "P2 Touches the ball " << p2touches <<  endl;
				if (p2touches >= 4) {
					bodyUserData1->game->player1Score++;
					bodyUserData1->game->reset();
					p2touches = 0;
				}
			}
		}
		

		//static_cast<Ball*>(bodyUserData)->startContact();
		//cout << &bodyUserData2 << endl;
	}
}
